// import { Socket } from "net";
import express, {Request, Response} from 'express';
import { createServer } from "http";
import socketio, { Server, Socket } from "socket.io";
const app = express();
const port = 3000;
const httpServer = createServer(app);
let clients = 0;
const DIRECTION = ['left','right','up','down'];
const io = require("socket.io")(httpServer, {
    cors: {
        origin: "http://localhost:8080",
        methods: ["GET", "POST"],
        allowedHeaders: ["my-custom-header"],
        credentials: true
    },
    allowEIO3: true
});
httpServer.listen(port);
io.on("connection", (socket: Socket) => {
    clients++;
    socket.emit('newclientconnect',{ description: 'Hey, welcome!'});
    socket.broadcast.emit('newclientconnect',{ description: clients + ' clients connected!'})
    socket.on('disconnect', function () {
        clients--;
        socket.broadcast.emit('newclientconnect',{ description: clients + ' clients connected!'})
    });
    socket.on('message', (data) => {
        socket.emit('message', '嗨 ,'+ data);
    });
    socket.on('client-send-move',(data)=>{
        if (!data.playerID) return;
        if (!DIRECTION.includes(data.direction)) return;

        socket.emit('server-move', data);
        // console.log(data.direction, DIRECTION.includes(data.direction))
    });
    socket.emit("setPlayerData", { position:{x:200, y:200}, playerID: clients});
});
app.get('/', (req: Request, res: Response) => {
    // res.send("hi");
});