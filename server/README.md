## 初始化專案 建立package.json
- terminal指令
    ```
    npm init
    ```

    ## 安裝express與typescript
    ```
    npm install --save-dev typescript @types/express @types/node

    npm install --save express
    ```

    ```
    tsc --init
    ```
    會產生 tsconfig.json，當 TypeScript 透過 tsc 編譯成 JavaScript 時，就會遵循該當暗的設定進行編譯。
    src:https://www.typescriptlang.org/tsconfig#exclude

- 創建 src 資料夾與app.ts
    ```
    mkdir src
    touch app.ts
    ```
- 安裝nodemon
`npm install nodemon` or `npm install -g nodemon` 
-g or --global: install the package globally rather than locally.
package.json "scripts"下加入 "start": "nodemon src/app.ts"